-- FUNCTION: blog.update_blog(integer, text, integer, integer, boolean, date, text)

-- DROP FUNCTION blog.update_blog(integer, text, integer, integer, boolean, date, text);

CREATE OR REPLACE FUNCTION blog.update_blog(
	id integer,
	name text,
	user_id integer,
	version integer,
	isdeleted boolean,
	"Date" date,
	type text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 0
AS $BODY$

UPDATE blog."blog"
	SET name=$2, user_id=$3, version=$4, isdeleted=$5, "Date"=$6, type=$7
	WHERE id = $1
returning id;

$BODY$;

ALTER FUNCTION blog.update_blog(integer, text, integer, integer, boolean, date, text)
    OWNER TO postgres;


-- FUNCTION: blog.update_user(bigint, text, text, date, text, text, text, text)

-- DROP FUNCTION blog.update_user(bigint, text, text, date, text, text, text, text);

CREATE OR REPLACE FUNCTION blog.update_user(
	id bigint,
	first_name text,
	second_name text,
	birth_date date,
	email text,
	role text,
	login text,
	password text)
    RETURNS bigint
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 0
AS $BODY$

UPDATE blog."User"
	SET first_name=$2, second_name=$3, birth_date=$4, email=$5, "Role"=$6, login=$7, "Password"=$8
	WHERE id = $1
returning id;

$BODY$;

ALTER FUNCTION blog.update_user(bigint, text, text, date, text, text, text, text)
    OWNER TO postgres;


		-- FUNCTION: blog.update_publication(integer, text, text, integer, integer, boolean, date, text)

		-- DROP FUNCTION blog.update_publication(integer, text, text, integer, integer, boolean, date, text);

CREATE OR REPLACE FUNCTION blog.update_publication(
	id integer,
    "Text" text,
	name text,
	blog_id integer,
	version integer,
	isdeleted boolean,
	"Date" date,
	type text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$

UPDATE blog."publication"
	SET "Text" = $2, name=$3, blog_id=$4, version=$5, isdeleted=$6, "Date"=$7, type=$8
	WHERE id = $1
returning id;

$BODY$;

ALTER FUNCTION blog.update_publication(integer, text, text, integer, integer, boolean, date, text)
    OWNER TO postgres;
