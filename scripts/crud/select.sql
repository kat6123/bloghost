-- FUNCTION: blog.select_blogs(integer)

DROP FUNCTION blog.select_blogs(integer);

CREATE OR REPLACE FUNCTION blog.select_blogs(
	id_user integer)
    RETURNS TABLE(id integer, name text, user_id integer, version integer, isdeleted boolean, "Date" date, type text)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

select * from blog.blog
where blog.blog.user_id = id_user and blog.blog.isdeleted <> 't';

$BODY$;

ALTER FUNCTION blog.select_blogs(integer)
    OWNER TO postgres;


		DROP FUNCTION blog.select_publics_by_blog_id(integer);

		CREATE OR REPLACE FUNCTION blog.select_publics_by_blog_id(
			id_blog integer)
		    RETURNS TABLE(id integer,
		                  "Text" text,
		                  name text,
		                  blog_id integer,
		                  version integer,
		                  "Date" date,
		                  type text,
		    			  user_id bigint,
		                  user_name text)
		    LANGUAGE 'sql'

		    COST 100
		    VOLATILE
		    ROWS 1000
		AS $BODY$

		select pb.id,
			   pb."Text",
		       pb.name,
		       pb.blog_id,
		       pb.version,
		       pb."Date",
		       pb."type",
		       us.id as user_id,
		       concat(coalesce(us.first_name, ' '),' ', coalesce(us.second_name, ' ')) as user_name
		from blog.publication as pb
		join blog.blog as bl on bl.id = pb.blog_id
		join blog."User" as us on us.id = bl.user_id
		where pb.blog_id = id_blog and pb.isdeleted <> 't'
		order by pb."Date" desc ;

		$BODY$;



-- FUNCTION: blog.select_users()

-- DROP FUNCTION blog.select_users();

CREATE OR REPLACE FUNCTION blog.select_users(
	)
    RETURNS TABLE(id bigint, first_name text, second_name text, birth_date date, email text, role text, login text, password text)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

select * from blog."User"
order by login;

$BODY$;

ALTER FUNCTION blog.select_users()
    OWNER TO postgres;


	-- FUNCTION: blog.select_publication()

	--DROP FUNCTION blog.select_publication();

	CREATE OR REPLACE FUNCTION blog.select_publication(
		)
	    RETURNS TABLE(
	        id integer,
	        "Text" text,
	        name text,
	        blog_id integer,
	        version integer,
	        "Date" date,
	        "type" text,
	        user_id bigint,
	        user_name text)
	    LANGUAGE 'sql'

	    COST 100
	    VOLATILE
	    ROWS 1000
	AS $BODY$

	select pb.id,
		   pb."Text",
	       pb.name,
	       pb.blog_id,
	       pb.version,
	       pb."Date",
	       pb."type",
	       us.id as user_id,
	       us.second_name as user_name
	from blog."publication" as pb
	join blog.blog as bl on bl.id = pb.blog_id
	join blog."User" as us on us.id = bl.user_id
	where pb.isdeleted <> 't'
	order by "Date" desc;

	$BODY$;


	-- FUNCTION: blog.select_comments_by_pub_id(integer)

	DROP FUNCTION blog.select_comments_by_pub_id(integer);

	CREATE OR REPLACE FUNCTION blog.select_comments_by_pub_id(publication_id integer
		)
	    RETURNS TABLE(id integer, "Text" text, "Date" date, user_id bigint, user_name text)
	    LANGUAGE 'sql'

	    COST 100
	    VOLATILE
	    ROWS 1000
	AS $BODY$

	select
	       cm.id,
	       cm."Text",
	       cm."Date",
	       us.id as user_id,
	       concat(coalesce(us.first_name, ' '),' ', coalesce(us.second_name, ' ')) as user_name
	from blog."Comment" as cm
	join blog."User" as us on us.id = cm.user_id
	where cm.isdeleted <> 't' and cm.publication_id = $1
	order by "Date" desc;

	$BODY$;

	ALTER FUNCTION blog.select_comments_by_pub_id(integer)
	    OWNER TO postgres;
