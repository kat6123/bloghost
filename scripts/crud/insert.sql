-- FUNCTION: blog.create_user_v2(text, text, date, text, text, text, text)

DROP FUNCTION blog.create_user(text, text, date, text, text, text, text);

CREATE OR REPLACE FUNCTION blog.create_user(
	first_name text DEFAULT 'Name'::text,
	second_name text DEFAULT 'Surname'::text,
	birth_date date DEFAULT NULL::date,
	email text DEFAULT NULL::text,
	role text DEFAULT 'user'::text,
	login text DEFAULT NULL::text,
	password text DEFAULT NULL::text)
    RETURNS TABLE(id bigint, role text, user_name text)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

INSERT INTO blog."User"(
	first_name, second_name, birth_date, email, "Role", login, "Password")
	VALUES ($1, $2, $3, $4, $5, $6, $7);
SELECT
 us.id, us."Role", concat(coalesce(us.first_name, ' '),' ', coalesce(us.second_name, ' ')) as user_name
 FROM
 blog."User" as us
 WHERE
 login = $6;

$BODY$;

-- FUNCTION: blog.create_blog(text, integer, integer, boolean, date, text)

-- DROP FUNCTION blog.create_blog(text, integer, integer, boolean, date, text);

CREATE OR REPLACE FUNCTION blog.create_blog(
	name text,
	user_id integer,
   	date date DEFAULT CURRENT_DATE::date,
	version integer DEFAULT 1::integer,
	isdeleted boolean DEFAULT false::boolean,
    type text  DEFAULT 'blog'::text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$

INSERT INTO blog.blog(
	"name", user_id, "version", isdeleted, "Date", "type")
	VALUES ($1, $2, $4, $5, $3, $6)
    RETURNING id;

$BODY$;


-- FUNCTION: blog.create_comment(date, text, bigint, bigint, bigint, text)

-- DROP FUNCTION blog.create_comment(date, text, bigint, bigint, bigint, text);

CREATE OR REPLACE FUNCTION blog.create_comment(
	text text,
	user_id bigint,
	publication_id bigint,
	publication_blog_idblog bigint,
    date date DEFAULT CURRENT_DATE::date,
	type text DEFAULT 'comment'::text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE

AS $BODY$

INSERT INTO blog."Comment"(
	"Date", "Text", user_id, publication_id, publication_blog_idblog, "type")
	VALUES ($5, $1, $2, $3, $4, $6)
    RETURNING id;

$BODY$;

-- FUNCTION: blog.create_publication(text, text, integer, integer, boolean, date, text)

-- DROP FUNCTION blog.create_publication(text, text, integer, integer, boolean, date, text);

CREATE OR REPLACE FUNCTION blog.create_publication(
	text text,
	name text,
	blog_id integer,
	version integer DEFAULT 1::integer,
	isdeleted boolean DEFAULT false::boolean,
	date date DEFAULT CURRENT_DATE::date,
	type text DEFAULT 'post'::text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$

INSERT INTO blog.publication(
	"Text", "name", blog_id, "version", isdeleted, "Date", "type")
	VALUES ($1, $2, $3, $4, $5, $6, $7)
    RETURNING id;

$BODY$;


-- FUNCTION: blog."create_Tag"(text)

-- DROP FUNCTION blog."create_Tag"(text);

CREATE OR REPLACE FUNCTION blog.create_Tag(
	name text)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 0
AS $BODY$

INSERT INTO blog."Тag"(
	name)
	VALUES ($1)
    returning id;

$BODY$;


-- FUNCTION: blog."create_user_has_Tag"(integer, integer, integer)

-- DROP FUNCTION blog."create_user_has_Tag"(integer, integer, integer);

CREATE OR REPLACE FUNCTION blog.create_user_has_Tag(
	user_id integer,
	tag_id integer,
	publication_id integer)
    RETURNS void
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 0
AS $BODY$

INSERT INTO blog."user_has_Тag"(
	user_id, "Тag_id", publication_id)
	VALUES ($1, $2, $3)
    ;

$BODY$;
