-- FUNCTION: blog.delete_user(bigint)

-- DROP FUNCTION blog.delete_user(bigint);

CREATE OR REPLACE FUNCTION blog.delete_user(
	id bigint)
    RETURNS void
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
    ROWS 0
AS $BODY$

delete from blog."User" where id = $1;

$BODY$;

ALTER FUNCTION blog.delete_user(bigint)
    OWNER TO postgres;
