drop SCHEMA if EXISTS blog cascade;
create schema blog;
SET search_path TO blog;

CREATE TABLE IF NOT EXISTS "User"(
	ID BIGSERIAL PRIMARY KEY,
	First_name VARCHAR(45) NULL,
    Second_name VARCHAR(45) NULL,
    Birth_date DATE NULL,
    Email VARCHAR(45) NULL,
    "Role" VARCHAR(45) NULL,
    Login VARCHAR(45) NOT NULL,
    "Password" VARCHAR(45) NOT NULL,
		CONSTRAINT pk PRIMARY KEY (User_id, Тag_id, Publication_id),
		CONSTRAINT unique_login PRIMARY KEY (login),
);

CREATE TABLE IF NOT EXISTS Blog (
    ID  SERIAL PRIMARY KEY,
    Name VARCHAR(45) NULL DEFAULT 'New blog',
    User_ID INT NOT NULL,
    Version INT NOT NULL DEFAULT 1,
    IsDeleted BOOLEAN NOT NULL DEFAULT FALSE,
    "Date" DATE NOT NULL,
    Type VARCHAR(45) NULL DEFAULT 'Blog',
    CONSTRAINT fk_Blog_User1 FOREIGN KEY (User_ID) REFERENCES "User"(ID)
);

CREATE TABLE IF NOT EXISTS Publication(
    ID Serial PRIMARY KEY,
    "Text" Text NULL,
    Name VARCHAR(45) NULL,
    Blog_ID INT NOT NULL,
    Version INT NOT NULL DEFAULT 1,
    IsDeleted BOOLEAN NOT NULL DEFAULT FALSE,
    "Date" DATE NOT NULL,
    Type VARCHAR(45) NULL DEFAULT 'Publication',
    CONSTRAINT fk_Publication_Blog1 FOREIGN KEY (Blog_ID) REFERENCES Blog(ID)
);

CREATE TABLE IF NOT EXISTS Тag (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(45) NULL
);


CREATE TABLE IF NOT EXISTS "Comment" (
    ID SERIAL PRIMARY KEY,
    "Date" Date NULL,
    "Text" Text NULL,
    User_ID INT NOT NULL,
    Publication_ID INT NOT NULL,
    Publication_Blog_idBlog INT NOT NULL,
    Type VARCHAR(45) NULL DEFAULT 'Comment',
    CONSTRAINT fk_Comment_User1 FOREIGN KEY (User_ID) REFERENCES "User"(ID),
    CONSTRAINT fk_Comment_Publication1 FOREIGN KEY (Publication_ID)
    	REFERENCES Publication(ID)
);


CREATE TABLE IF NOT EXISTS Record(
    ID SERIAL PRIMARY KEY,
    Action VARCHAR(45) NULL,
    Datetime VARCHAR(45) NULL,
    User_ID INT NOT NULL,
    Type VARCHAR(45) NULL
);


CREATE TABLE IF NOT EXISTS User_has_Тag (
    User_id INT NOT NULL,
    Тag_id INT NOT NULL,
    Publication_id INT NOT NULL,

    CONSTRAINT pk PRIMARY KEY (User_id, Тag_id, Publication_id),

    CONSTRAINT fk_User_has_Тag_User1 FOREIGN KEY (User_id) REFERENCES "User"(ID),
    CONSTRAINT fk_User_has_Тag_Тag1 FOREIGN KEY (Тag_id) REFERENCES Тag(ID),
    CONSTRAINT fk_User_has_Тag_Publication1 FOREIGN KEY (Publication_id)
    	REFERENCES Publication(ID)
);

ALTER TABLE blog."Comment" DROP CONSTRAINT fk_comment_publication1;

ALTER TABLE blog."Comment"
    ADD CONSTRAINT fk_comment_publication1 FOREIGN KEY (publication_id)
    REFERENCES blog.publication (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."Comment" DROP CONSTRAINT fk_comment_user1;

ALTER TABLE blog."Comment"
    ADD CONSTRAINT fk_comment_user1 FOREIGN KEY (user_id)
    REFERENCES blog."User" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."blog" DROP CONSTRAINT fk_blog_user1;

ALTER TABLE blog."blog"
    ADD CONSTRAINT fk_blog_user1 FOREIGN KEY (user_id)
    REFERENCES blog."User" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."publication" DROP CONSTRAINT fk_publication_blog1;

ALTER TABLE blog."publication"
    ADD CONSTRAINT fk_publication_blog1 FOREIGN KEY (blog_id)
    REFERENCES blog.blog (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."user_has_Тag" DROP CONSTRAINT "fk_user_has_Тag_publication1";

ALTER TABLE blog."user_has_Тag"
    ADD CONSTRAINT "fk_user_has_Тag_publication1" FOREIGN KEY (publication_id)
    REFERENCES blog.publication (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."user_has_Тag" DROP CONSTRAINT "fk_user_has_Тag_user1";

ALTER TABLE blog."user_has_Тag"
    ADD CONSTRAINT "fk_user_has_Тag_user1" FOREIGN KEY (user_id)
    REFERENCES blog."User" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."user_has_Тag" DROP CONSTRAINT "fk_user_has_Тag_Тag1";

ALTER TABLE blog."user_has_Тag"
    ADD CONSTRAINT "fk_user_has_Тag_Тag1" FOREIGN KEY ("Тag_id")
    REFERENCES blog."Тag" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE blog."Comment"
ADD COLUMN isdeleted boolean DEFAULT false;

ALTER TABLE blog.record
ALTER COLUMN user_id TYPE bigint ;
ALTER TABLE blog.record
ALTER COLUMN user_id DROP NOT NULL; 
