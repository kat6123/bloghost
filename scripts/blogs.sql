DELETE from blog.blog;
--1
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 1, 'Recipe of India', '2017-11-11', 'cook');
--2
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 1, 'SAP', '2017-10-11', 'work');
--3
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 2, 'Recipe of Belarusian cuisine', '2017-08-20', 'cook');
--4
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 3, 'School of data analysis', '2017-09-01', 'study');
--5
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 4, 'WhatWhereWhen', '2017-06-11', 'entertainment');
--6
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 4, 'JS', '2016-12-15', 'course');
--7
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 5, 'IBA', '2017-11-05', 'course');
--8
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 5, 'Python', '2017-02-13', 'study');
--9
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 6, 'New Year', '2017-12-15', 'holiday');
--10
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 7, 'Christmas', '2017-12-20', 'holiday');
--11
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 8, 'Music', '2017-12-20', 'entertainment');
--12
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 9, 'Easter', '2017-04-15', 'holiday');
--13
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 10, 'Math', '2017-08-08', 'study');
--14
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 10, 'Holiday Meal', '2017-08-08', 'cook');
--15
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 12, 'Cinema', '2017-12-20', 'entertainment');
--16
INSERT INTO blog.blog (
version, user_id, name, "Date", type) VALUES (
1, 13, 'Theatre', '2017-12-20', 'entertainment');
