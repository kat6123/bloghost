DELETE from blog.blog;

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 1, 'Recipe of India', '2017-11-11', 'cook', 1);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 1, 'SAP', '2017-10-11', 'work', 2);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 2, 'Recipe of Belarusian cuisine', '2017-08-20', 'cook', 3);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 3, 'School of data analysis', '2017-09-01', 'study', 4);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 4, 'WhatWhereWhen', '2017-06-11', 'entertainment', 5);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 4, 'JS', '2016-12-15', 'course', 6);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 5, 'IBA', '2017-11-05', 'course', 7);

INSERT INTO blog.blog (
version, user_id, name, "Date", type, id) VALUES (
1, 5, 'Python', '2017-02-13', 'study', 8);
