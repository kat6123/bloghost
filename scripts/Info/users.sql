DELETE from blog."User";

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Anastasiya', 'Kozlova', 'nastya.kozlova@gmail.com', 'admin', '1997-09-15', 'Nastya', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Katya', 'Shilovskaya', 'kat.shil@gmail.com', 'admin', '1998-08-21', 'Kat', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Inna', 'Rudovich', 'inna.rud@gmail.com', 'admin', '1998-01-02', 'Inna', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Marina', 'Korneichuk', 'marina.kor@gmail.com', 'user', '1998-01-01', 'Marina', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Veronica', 'Voronova', 'ver.vor@gmail.com', 'user', '1998-05-30', 'Ver', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Vlad', 'Turlay', 'vlad.tur@gmail.com', 'user', '1998-09-01', 'Vlad', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Stas', 'Gluboky', 'stas.gl@gmail.com', 'user', '1998-01-01', 'Stas', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Anastasiya', 'Kornilo', 'kornilo@gmail.com', 'user', '1998-03-01', 'NastyaKornilo', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Lera', 'Aleshko', 'lera.al@gmail.com', 'user', '1998-08-03', 'Lera', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Marina', 'Smantcer', 'smantcer@gmail.com', 'user', '1998-10-10', 'MarinaSmantcer', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Sasha', 'Streshinski', 'stresh@gmail.com', 'user', '1998-10-10', 'Stresh', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Lesha', 'Safronov', 'safronov@gmail.com', 'user', '1998-09-10', 'Safronov', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Zhenya', 'Gusakovskaya', 'zhen.gus@gmail.com', 'user', '1998-10-10', 'Gusakovskaya', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Vlad', 'Punko', 'punko@gmail.com', 'user', '1998-10-10', 'VladPunko', '202cb962ac59075b964b07152d234b70');

INSERT INTO blog."User" (
first_name, second_name, email, "Role", birth_date, login, "Password") VALUES (
'Ilya', 'Yakush', 'yakush@gmail.com', 'user', '1998-10-10', 'Yakush', '202cb962ac59075b964b07152d234b70');
