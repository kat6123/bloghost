DELETE from blog.publication;

INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
1, 'Soan Papdi', 1, '2017-11-11', 'Soan papdi (also known as patisa, son papri, 
    sohan papdi or shonpapri) [2] is a popular North Indian dessert. It is usually 
    cube-shaped or served as flakes, and has a crisp and flaky texture', 1);
    
INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
1, 'Khaman', 1, '2017-11-13', 'Khaman is a food common in the Gujarat state of 
    India made from soaked and freshly ground channa dal or channa flour (also 
    called gram flour or besan)', 2);
    
INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
3, 'Draniky', 1, '2017-08-25', 'Potato pancakes, latkes or boxties are 
    shallow-fried pancakes of grated or ground potato, flour and egg, often 
    flavored with grated garlic or onion and seasoning. They may be topped 
    with a variety of condiments, ranging from the savory (such as sour cream 
    or cottage cheese), to the sweet (such as apple sauce or sugar), or they 
    may be served plain. The dish is sometimes made from mashed potatoes to 
    make pancake-shaped croquettes', 3);
    
INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
3, 'Borscht', 1, '2017-11-01', 'Borscht is a sour soup popular in several Eastern 
    European cuisines, including Ukrainian, Armenian, Russian, Polish, Belarusian, 
    Lithuanian, Romanian and Ashkenazi Jewish cuisines. The variety most commonly 
    associated with the name in English is of Ukrainian origin and includes beetroots 
    as one of the main ingredients, which gives the dish a distinctive red color', 4);
    
INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
3, 'Pancake', 1, '2017-10-09', 'A pancake (or hotcake, griddlecake, or flapjack) is a 
    flat cake, often thin and round, prepared from a starch-based batter that may contain 
    eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, 
    often frying with oil or butter. In Britain, pancakes are often unleavened and resemble 
    a crepe. In North America, a leavening agent is used (typically baking powder)', 5);
    
INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text", id) VALUES (
8, 'Python', 1, '2017-07-06', 'Python is a widely used high-level programming language for 
    general-purpose programming, created by Guido van Rossum and first released in 1991. An 
    interpreted language, Python has a design philosophy that emphasizes code readability 
    (notably using whitespace indentation to delimit code blocks rather than curly brackets 
    or keywords), and a syntax that allows programmers to express concepts in fewer lines of 
    code than might be used in languages such as C++ or Java.', 6);