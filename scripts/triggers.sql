-- FUNCTION: blog.delete_update_insert_blog()

DROP FUNCTION blog.delete_update_insert_blog() CASCADE;

CREATE FUNCTION blog.delete_update_insert_blog()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 BEGIN
 IF (TG_OP = 'DELETE') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Delete blog', now(), Old.user_id;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            IF (New.isdeleted = 't') THEN
            	INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Delete blog', now(), New.user_id;
            ELSE
            	INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Update blog', now(), New.user_id;
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO blog.record("action", datetime,user_id ) SELECT 'Insert blog', now(), New.user_id;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
        END;

$BODY$;

ALTER FUNCTION blog.delete_update_insert_blog()
    OWNER TO postgres;

-- Trigger: delete/update/insert publication
CREATE TRIGGER "delete/update/insert blog"
    AFTER INSERT OR DELETE OR UPDATE
    ON blog.blog
    FOR EACH ROW
    EXECUTE PROCEDURE blog.delete_update_insert_blog();


-- FUNCTION: blog.delete_update_insert_publication()

DROP FUNCTION blog.delete_update_insert_publication() CASCADE;

CREATE FUNCTION blog.delete_update_insert_publication()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare
	user_id integer;
BEGIN

 IF (TG_OP = 'DELETE') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT
            								'Delete publication', now(), (select blog.user_id
                                                                          from blog.blog
                                                                          where blog.id = old.blog_id);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            IF (New.isdeleted = 't') THEN
            	INSERT INTO blog.record ("action", datetime,user_id ) SELECT
                									'Delete publication', now(), (select blog.user_id
                                                                          from blog.blog
                                                                          where blog.id = old.blog_id);
            ELSE
            	 INSERT INTO blog.record ("action", datetime,user_id ) SELECT
            								'Update publication', now(), (select blog.user_id
                                                                          from blog.blog
                                                                          where blog.id = New.blog_id);
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT
            								'Insert publication', now(), (select blog.user_id
                                                                          from blog.blog
                                                                          where blog.id = New.blog_id);
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
        END;

$BODY$;

ALTER FUNCTION blog.delete_update_insert_publication()
    OWNER TO postgres;


-- Trigger: delete/update/insert publication

CREATE TRIGGER "delete/update/insert publication"
    AFTER INSERT OR DELETE OR UPDATE
    ON blog.publication
    FOR EACH ROW
    EXECUTE PROCEDURE blog.delete_update_insert_publication();


    -- FUNCTION: blog.delete_update_insert_comment()

    -- DROP FUNCTION blog.delete_update_insert_comment();

    CREATE FUNCTION blog.delete_update_insert_comment()
        RETURNS trigger
        LANGUAGE 'plpgsql'
        COST 100
        VOLATILE NOT LEAKPROOF
        ROWS 0
    AS $BODY$
    BEGIN
     IF (TG_OP = 'DELETE') THEN
                INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Delete comment', now(), Old.user_id;
                RETURN OLD;
            ELSIF (TG_OP = 'UPDATE') THEN
                INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Update comment', now(), New.user_id;
                RETURN NEW;
            ELSIF (TG_OP = 'INSERT') THEN
                INSERT INTO blog.record ("action", datetime,user_id ) SELECT  'Insert comment', now(), New.user_id;
                RETURN NEW;
            END IF;
            RETURN NULL; -- result is ignored since this is an AFTER trigger
            END;

    $BODY$;


-- Trigger: delete/update/insert comment

-- DROP TRIGGER "delete/update/insert comment" ON blog."Comment";

CREATE TRIGGER "delete/update/insert comment"
    AFTER INSERT OR DELETE OR UPDATE
    ON blog."Comment"
    FOR EACH ROW
    EXECUTE PROCEDURE blog.delete_update_insert_comment();


  -- FUNCTION: blog.delete_update_insert_user()

  -- DROP FUNCTION blog.delete_update_insert_user();

  CREATE FUNCTION blog.delete_update_insert_user()
      RETURNS trigger
      LANGUAGE 'plpgsql'
      COST 100
      VOLATILE NOT LEAKPROOF
      ROWS 0
  AS $BODY$
   BEGIN
   IF (TG_OP = 'DELETE') THEN
              INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Delete user', now(), Old.id;
              RETURN OLD;
          ELSIF (TG_OP = 'UPDATE') THEN
              INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Update user', now(), New.id;
              RETURN NEW;
          ELSIF (TG_OP = 'INSERT') THEN
              INSERT INTO blog.record ("action", datetime,user_id ) SELECT  'Insert user', now(), New.id;
              RETURN NEW;
          END IF;
          RETURN NULL; -- result is ignored since this is an AFTER trigger
          END;

  $BODY$;


-- Trigger: delete/update/insert trigger

-- DROP TRIGGER "delete/update/insert trigger" ON blog."User";

CREATE TRIGGER "delete/update/insert trigger"
    AFTER INSERT OR DELETE OR UPDATE
    ON blog."User"
    FOR EACH ROW
    EXECUTE PROCEDURE blog.delete_update_insert_user();


-- FUNCTION: blog.delete_update_insert_tag_to_publication_by_user()

-- DROP FUNCTION blog.delete_update_insert_tag_to_publication_by_user();

CREATE FUNCTION blog.delete_update_insert_tag_to_publication_by_user()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
    ROWS 0
AS $BODY$
BEGIN
 IF (TG_OP = 'DELETE') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Delete tag', now(), Old.user_id;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT 'Update tag', now(), New.user_id;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO blog.record ("action", datetime,user_id ) SELECT  'Insert tag', now(), New.user_id;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
        END;

$BODY$;



-- Trigger: delete/update/insert tag

-- DROP TRIGGER "delete/update/insert tag" ON blog.user_has_Tag;


CREATE TRIGGER "delete/update/insert tag from user to publ"
    AFTER INSERT OR DELETE OR UPDATE
    ON blog."user_has_Тag"
    FOR EACH ROW
    EXECUTE PROCEDURE blog.delete_update_insert_tag_to_publication_by_user();
