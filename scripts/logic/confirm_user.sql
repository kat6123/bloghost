DROP FUNCTION blog.confirm_user(character varying,text);
CREATE OR REPLACE FUNCTION blog.confirm_user (_login VARCHAR, _password text) 
 RETURNS TABLE (
 id bigint,
 "role" varchar,
     user_name text
) 
AS $$
BEGIN
 RETURN QUERY SELECT
 u.id,
 u."Role",
 concat(coalesce(u.first_name, ' '),' ', coalesce(u.second_name, ' ')) as user_name
 FROM
 blog."User" as u
 WHERE
 u."login" = _login and u."Password" = _password ;
END; $$ 
 
LANGUAGE 'plpgsql';

ALTER FUNCTION blog.confirm_user(character varying,text)
    OWNER TO postgres;

