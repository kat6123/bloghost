-- FUNCTION: blog.does_comment_belong_to_user(bigint, integer)

-- DROP FUNCTION blog.does_comment_belong_to_user(bigint, integer);

CREATE OR REPLACE FUNCTION blog.does_comment_belong_to_user(
	user_id bigint,
	id_ integer)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE 

AS $BODY$

   

select cm.id from blog."Comment" as cm
where cm.user_id = $1 and cm.id = $2;
    

$BODY$;

ALTER FUNCTION blog.does_comment_belong_to_user(bigint, integer)
    OWNER TO postgres;
