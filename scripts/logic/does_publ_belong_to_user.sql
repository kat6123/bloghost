-- FUNCTION: blog.does_publ_belong_to_user(bigint, integer)

-- DROP FUNCTION blog.does_publ_belong_to_user(bigint, integer);

CREATE OR REPLACE FUNCTION blog.does_publ_belong_to_user(
	user_id bigint,
	id_ integer)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
    
AS $BODY$

   

select pb.id from blog."publication" as pb
join blog."blog" as bl on bl.id = pb.blog_id
where bl.user_id = $1 and pb.id = $2;
    

$BODY$;

ALTER FUNCTION blog.does_publ_belong_to_user(bigint, integer)
    OWNER TO postgres;
