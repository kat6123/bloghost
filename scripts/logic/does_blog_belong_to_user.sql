-- FUNCTION: blog.does_blog_belong_to_user(bigint, integer)

--DROP FUNCTION blog.does_blog_belong_to_user(bigint, integer);

CREATE OR REPLACE FUNCTION blog.does_blog_belong_to_user(
	user_id bigint,
	id_ integer)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE 

AS $BODY$
  
select blog.id as blog_id from blog."blog" 
where blog.user_id = $1 and blog.id = $2;

$BODY$;

ALTER FUNCTION blog.does_blog_belong_to_user(bigint, integer)
    OWNER TO postgres;
