DELETE from blog.publication;


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
1, 'Soan Papdi', 1, '2017-11-11', 'Soan papdi (also known as patisa, son papri, 
    sohan papdi or shonpapri) [2] is a popular North Indian dessert. It is usually 
    cube-shaped or served as flakes, and has a crisp and flaky texture');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
1, 'Khaman', 1, '2017-11-13', 'Khaman is a food common in the Gujarat state of 
    India made from soaked and freshly ground channa dal or channa flour (also 
    called gram flour or besan)');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
3, 'Draniky', 1, '2017-08-25', 'Potato pancakes, latkes or boxties are 
    shallow-fried pancakes of grated or ground potato, flour and egg, often 
    flavored with grated garlic or onion and seasoning. They may be topped 
    with a variety of condiments, ranging from the savory (such as sour cream 
    or cottage cheese), to the sweet (such as apple sauce or sugar), or they 
    may be served plain. The dish is sometimes made from mashed potatoes to 
    make pancake-shaped croquettes');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
3, 'Borscht', 1, '2017-11-01', 'Borscht is a sour soup popular in several Eastern 
    European cuisines, including Ukrainian, Armenian, Russian, Polish, Belarusian, 
    Lithuanian, Romanian and Ashkenazi Jewish cuisines. The variety most commonly 
    associated with the name in English is of Ukrainian origin and includes beetroots 
    as one of the main ingredients, which gives the dish a distinctive red color');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
3, 'Pancake', 1, '2017-10-09', 'A pancake (or hotcake, griddlecake, or flapjack) is a 
    flat cake, often thin and round, prepared from a starch-based batter that may contain 
    eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, 
    often frying with oil or butter. In Britain, pancakes are often unleavened and resemble 
    a crepe. In North America, a leavening agent is used (typically baking powder)');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
8, 'Python', 1, '2017-07-06', 'Python is a widely used high-level programming language for 
    general-purpose programming, created by Guido van Rossum and first released in 1991. An 
    interpreted language, Python has a design philosophy that emphasizes code readability 
    (notably using whitespace indentation to delimit code blocks rather than curly brackets 
    or keywords), and a syntax that allows programmers to express concepts in fewer lines of 
    code than might be used in languages such as C++ or Java.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
2, 'SAP', 1, '2017-07-06', 'SAP SE (Systeme, Anwendungen und Produkte in der Datenverarbeitung;
    "Systems, Applications & Products in Data Processing") is a European multinational software
    corporation that makes enterprise software to manage business operations and customer relations.
    SAP is headquartered in Walldorf, Baden-Württemberg, with regional offices in 180
    countries. The company has over 335,000 customers in over 180 countries. The company is
    a component of the Euro Stoxx 50 stock market index.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
2, 'SAP PartnerEdge', 1, '2017-07-06', 'SAP products for small businesses and midsize companies
    are delivered through its global partner network. The SAP PartnerEdge programme, SAPs partner
    programme, offers a set of business enablement resources and program benefits to help partners
    including value-added resellers (VARs) and independent software vendors (ISVs) be profitable and
    successful in implementing, selling, marketing, developing and delivering SAP products to a broad
    range of customers');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
4, 'ShDA', 1, '2017-07-06', 'School of data analysis (SHAD) — free two-year program from the company
    "Yandex", opened in 2007. At School of data analysis teaching components of data science,
    Data Science. In the SHAD there are three sections: data analysis, computer science, and big data.
    For admission must pass through three stages: online test, written exam and the interview.
    The School program extends beyond the University and is designed for students older than second-year
    mathematical or technical faculty of the University. The set of courses are formed experimentally
    and is highly flexible, varying from year to year[1]. The average load is 15-20 hours per week, including
    9-12 hours of lessons with teachers. Per semester a student must pass at least 3 courses. The course
    grade consists of the sum of scores handed over during the semester homework.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
4, 'General info', 1, '2017-07-06', 'The main aim of data analysis is to prepare professionals for Yandex
     and for the it industry in General - in the field of data processing and analysis and extract information
     from the Internet. School Yandex is a two-year full-time evening courses that lead teachers of Russian
     and foreign universities. The school gives the opportunity to get an education in the topics of computer
     science that are not usually included in the curricula of universities. School Yandex is designed for
     students and graduates (post-graduates and young specialists) of engineering and mathematical specialties,
     prepared several times a week to attend evening classes. The approximate download size of the student approximately
     15-20 hours per week, including 9-12 hours of lessons with teachers. During the training or at its end, the students
     can do an internship in Yandex. During the internship students work under the guidance of the developers of Yandex,
     and teachers.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
5, 'WhatWhereWhen', 1, '2017-07-06', 'What? Where? When? (Russian: translit. Chto? Gde? Kogda?)
     is an intellectual game show well known in Russian-language media and other CIS states since the mid-1970s. Today
     it is produced for television by TV Igra on the Russian Channel One and also exists as a competitive game played in
     clubs organized by the World Association of Clubs. Over 21,000 teams worldwide play the sport version of the game,
     based on the TV show.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
5, 'WhatWhereWhen Facts', 1, '2017-07-06', 'The game is played between a "team of TV viewers" and a team of
    six experts. Viewers ask questions to the experts, and the experts, during a one-minute discussion, try
    to find the answer to the given question. If the experts answer the question correctly, they earn a point.
    If their answer is wrong, the viewers` team gets a point, and the viewer who sent in this question receives
    a monetary prize. The experts do not receive monetary prizes, except for the best player in case that they
    win the final game of the series or the year. The experts sit around the round table divided into 13 sectors,
    12 of which contain envelopes with questions mailed in by viewers and pre-checked for validity, while the 13th
    sector (see below) indicates a question randomly selected from questions submitted by Internet during the show.
    Questions from the 13th sector are not pre-checked thus their quality and validity are not guaranteed.
    The arrow on the spinning top selects the sector which will be played next. If the arrow points to a sector
    which has already been played, the next clockwise sector is selected.
    A question may involve material objects or media (video or audio) demonstrated to the players.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
5, 'History', 1, '2017-07-06', 'The game was developed between 1975 and 1977 by artist, television host and director
    Vladimir Voroshilov. The very first version of the game (aired September 4, 1975) emphasized knowledge rather than
    logic; two families competed from their homes. In the next two years only two games were aired, the second of
    which, on 24 December 1977, already was close to the today`s format: a top spinning on the table selects a viewer`s
    question which is discussed for one minute by a team of 6 persons; the host is "invisible" and present only as
    a voice. (At the time, Voroshilov was banned from appearing on the screen, even his name was not indicated in the
    show credits.) Since 1978, the game is aired regularly. The final major change in rules, in 1982, established that
    the game continues until 6 points are scored by either side. Since 1986, the games are broadcast live.
    The game quickly became popular, and a dozen or so of the best players from the TV version have become household
    names of the same magnitude as pop-music stars: Viktor Sidnev, Nurali Latypov, Alexander Drouz, Alexei Blinov,
    Fyodor Dvinyatin, Boris Burda, Anatoly Wasserman, Maxim Potashev and so on.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
6, 'Java Script', 1, '2017-07-06', 'JavaScript , often abbreviated as JS, is a high-level, dynamic, weakly typed,
    prototype-based, multi-paradigm, and interpreted programming language. Alongside HTML and CSS, JavaScript is
    one of the three core technologies of World Wide Web content production. It is used to make webpages interactive
    and provide online programs, including video games. The majority of websites employ it, and all modern web browsers
    support it without the need for plug-ins by means of a built-in JavaScript engine. Each of the many JavaScript
    engines represent a different implementation of JavaScript, all based on the ECMAScript specification, with
    some engines not supporting the spec fully, and with many engines supporting additional features beyond ECMA.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
6, 'History', 1, '2017-07-06', 'n 1993, the National Center for Supercomputing Applications (NCSA), a unit of
    the Universityof Illinois at Urbana-Champaign, released NCSA Mosaic, the first popular graphical Web browser,
    which played an important part in expanding the growth of the nascent World Wide Web. In 1994, a company called \
    Mosaic Communications was founded in Mountain View, California and employed many of the original NCSA Mosaic
    authors to create Mosaic Netscape. However, it intentionally shared no code with NCSA Mosaic. The internal
    codename for the company`s browser was Mozilla, which stood for "Mosaic killer", as the company`s goal was to
    displace NCSA Mosaic as the world`s number one web browser. The first version of the Web browser,
    Mosaic Netscape 0.9, was released in late 1994. Within four months it had already taken three-quarters of the
    browser market and became the main browser for the Internet in the 1990s. To avoid trademark ownership problems
    with the NCSA, the browser was subsequently renamed Netscape Navigator in the same year, and the company took
    the name Netscape Communications. Netscape Communications realized that the Web needed to become more dynamic.
    Marc Andreessen, the founder of the company believed that HTML needed a "glue language" that was easy to use
    by Web designers and part-time programmers to assemble components such as images and plugins, where the code
    could be written directly in the Web page markup.')


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
6, 'Java Script Functional', 1, '2017-07-06', 'A function is first-class; a function is considered to be an object.
    As such, a function may have properties and methods, such as .call() and .bind().[37] A nested function is a
    function defined within another function. It is created each time the outer function is invoked. In addition,
    each nested function forms a lexical closure: The lexical scope of the outer function (including any constant,
    local variable, or argument value) becomes part of the internal state of each inner function object, even after
    execution of the outer function concludes.[38] JavaScript also supports anonymous functions.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
7, 'IBA', 1, '2017-07-06', 'IBA Group, an Alliance of companies specializing in the field of information technology,
    developers, manufacturers and suppliers of solutions and services in the it field in the countries of Central
    and Eastern Europe.
    Under the brand IBA Group employs more than 20 companies located in the Czech Republic, Belarus, Russia, Germany,
    USA, Kazakhstan, UK, Ukraine, Slovakia, South Africa and Cyprus. In 2005 after the Czech Republic joined the
    European Union location of the head office of IBA Group became Prague.
    The activities of the IBA Group includes the implementation of projects in the field of system integration,
    software development (SOFTWARE), it consulting and training, production, and post-warranty service of computer
    equipment (SVT).
    IBA Group has completed more than 2,000 projects for clients, the scope of which applies to different sectors,
    including information technology, transport, telecommunications, energy.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
7, 'IBA history', 1, '2017-07-06', '1993 — Establishment of the IBA (JV "international business Alliance", Minsk,
    Belarus)
    1994 — Opening of the IBA Computing center (Minsk, Belarus)[5]
    1997 — joint venture "production Information architecture" — the IBA IS (Minsk, Belarus)
    1998 — opening of the IBA office USA (Mountain View, USA)
    1999 — opening of a development center, IBA CZ (Prague, Czech Republic); office of the IBA Development
    (Limassol, Cyprus); IBA Technical Center (Minsk, Belarus)
    2000 — Opening of the office of the IBA IT GmbH (Dusseldorf, Germany); opening of the Studio of Web-design
    PixelHead (Minsk, Belarus)
    2001 — opening of a development center of IBA Gomel (Gomel, Belarus)
    2002 — Opening of the IBA Training center (Minsk, Belarus); the IBA membership in the Association of
    Software Developers RUSSOFT
    2003 — Opening of the branch in IBA Novopolotsk, Belarus');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
9, 'Chinese New Year', 1, '2017-07-06', 'Chinese New Year, also known as the "Spring Festival"
    in modern China, is an important Chinese festival celebrated at the turn of the traditional
    lunisolar Chinese calendar. Celebrations traditionally run from the evening preceding the first day,
    to the Lantern Festival on the 15th day of the first calendar month. The first day of the New Year falls
    on the new moon between 21 Jan and 20 Feb. In 2017, the first day of the Chinese New Year was on
    Saturday, 28 January, initiating the year of the Rooster.
    The New Year festival is centuries old and gains significance because of several myths and customs.
    Traditionally, the festival was a time to honor deities as well as ancestors. Chinese New Year is
    celebrated in countries and territories with significant Chinese populations, including Mainland China,
    Taiwan, Hong Kong, Macau, Singapore, Indonesia, Malaysia, Thailand, Vietnam, Cambodia, Mauritius,
    Australia, and the Philippines. Chinese New Year is considered a major holiday for the Chinese and
    has had influence on the lunar new year celebrations of its geographic neighbors.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
9, 'British New Year', 1, '2017-07-06', 'British celebrations of New Year are a defined and precise
    reflection of the customs, cultures, and traditions as followed and hold by British people.
    However, with the New Year celebrations in British, one can clearly draw a prominent exception in the list.
    It is not that British totally evade itself from accepting the changing times, and the changing way of
    celebrations. But, they do change, but at the same time ensuring that they remain intact with their own
    customs and traditions even in the changing circumstances. New Year celebrations are a clear reflection of that.
    British celebrate New Year on January 1, i.e. the first date of the first month of the Georgian calendar.
    British celebrations reflect high vigor, enthusiasm, pleasure, and delight; and at the same times give a glance
    of their rich customs and traditions. They reflect the British belief that one should initiate anything with
    positive hopes and beliefs of pleasurable coming time.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
10, 'Christmas', 1, '2017-07-06', 'Christmas is an annual festival commemorating the birth of Jesus Christ,
    observed most commonly on December 25 as a religious and cultural celebration among billions of people
    around the world. A feast central to the Christian liturgical year, it is preceded by the season of
    Advent or the Nativity Fast and initiates the season of Christmastide, which historically in the West
    lasts twelve days and culminates on Twelfth Night; in some traditions, Christmastide includes an Octave.
    Christmas Day is a public holiday in many of the world`s nations, is celebrated religiously by a majority
    of Christians,[18] as well as culturally by many non-Christians, and forms an integral part of the holiday
    season. In several countries, celebrating Christmas Eve has the main focus rather than Christmas Day');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
10, 'Traditions', 1, '2017-07-06', 'Christmas Day is celebrated as a major festival and public holiday in
    countries around the world, including many whose populations are mostly non-Christian. In some
    non-Christian areas, periods of former colonial rule introduced the celebration (e.g. Hong Kong);
    in others, Christian minorities or foreign cultural influences have led populations to observe the
    holiday. Countries such as Japan, where Christmas is popular despite there being only a small number
    of Christians, have adopted many of the secular aspects of Christmas, such as gift-giving, decorations,
    and Christmas trees.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
10, 'Traditions 2', 1, '2017-07-06', 'Among countries with a strong Christian tradition, a variety of
    Christmas celebrations have developed that incorporate regional and local cultures. For Christians,
    participating in a religious service plays an important part in the recognition of the season. Christmas,
    along with Easter, is the period of highest annual church attendance. In Catholic countries, people hold
    religious processions or parades in the days preceding Christmas. In other countries, secular processions
    or parades featuring Santa Claus and other seasonal figures are often held. Family reunions and the
    exchange of gifts are a widespread feature of the season. Gift giving takes place on Christmas Day
    in most countries. Others practice gift giving on December 6, Saint Nicholas Day, and January 6, Epiphany.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
11, 'Pop', 1, '2017-07-06', 'Pop music is a genre of popular music that originated in its modern form in
    the United States and United Kingdom during the mid-1950s.[4] The terms "popular music" and "pop music"
    are often used interchangeably, although the former describes all music that is popular and includes many
    different styles. "Pop" and "rock" were roughly synonymous terms until the late 1960s, when they became
    increasingly differentiated from each other.
    Although much of the music that appears on record charts is seen as pop music, the genre is distinguished
    from chart music. Pop music is eclectic, and often borrows elements from other styles such as urban, dance,
    rock, Latin, and country; nonetheless, there are core elements that define pop music. Identifying factors
    include generally short to medium-length songs written in a basic format (often the verse-chorus structure),
    as well as common use of repeated choruses, melodic tunes, and hooks.');


INSERT INTO blog.publication (
blog_id, name, version, "Date", "Text") VALUES (
11, 'Rock', 1, '2017-07-06', 'Rock music is a broad genre of popular music that originated as "rock and roll"
    in the United States in the early 1950s, and developed into a range of different styles in the 1960s and
    later, particularly in the United Kingdom and in the United States. It has its roots in 1940s and 1950s
    rock and roll, a style which drew heavily on the African-American genres of blues and rhythm and blues,
    and from country music. Rock music also drew strongly on a number of other genres such as electric blues
    and folk, and incorporated influences from jazz, classical and other musical styles. Musically, rock has
    centered on the electric guitar, usually as part of a rock group with electric bass and drums and one or
    more singers. Typically, rock is song-based music usually with a 4/4 time signature using a verse–chorus
    form, but the genre has become extremely diverse. Like pop music, lyrics often stress romantic love but
    also address a wide variety of other themes that are frequently social or political.');

