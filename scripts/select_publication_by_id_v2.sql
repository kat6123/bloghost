-- FUNCTION: blog.select_publication_by_id(integer)
DROP FUNCTION blog.select_publication_by_id(integer);

CREATE OR REPLACE FUNCTION blog.select_publication_by_id(
	publication_id integer)
    RETURNS TABLE(
        		  id integer,  
        		  "Text" text, 
                  name text, 
                  blog_id integer,
        		  version integer,
                  "Date" date, 
        		  type text,
                  user_id bigint, 
                  user_name text, 
                  blog_name text) 
LANGUAGE 'sql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

select pb.id,
	   pb."Text",
       pb.name,
       pb.blog_id,
       pb.version,
       pb."Date",
       pb."type",
       us.id as user_id, 
       concat(coalesce(us.first_name, ' '),' ', coalesce(us.second_name, ' ')) as user_name,
       bl.name
from blog."publication" as pb
join blog.blog as bl on bl.id = pb.blog_id
join blog."User" as us on us.id = bl.user_id
where pb.isdeleted <> 't' and pb.id = publication_id
order by "Date" desc;
$BODY$;

ALTER FUNCTION blog.select_publication_by_id(integer)
    OWNER TO postgres;
