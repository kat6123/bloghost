# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, redirect
from django.conf import settings
from forms import BlogForm, PostForm, UserForm

import psycopg2
import hashlib
import datetime


def does_publ_belong_to_user(user_id, post_id):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
            cursor.callproc('blog.does_publ_belong_to_user', (user_id, post_id))
            result = cursor.fetchone()

    return False if result[0] is None else True


def does_blog_belong_to_user(user_id, blog_id):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
            cursor.callproc('blog.does_blog_belong_to_user', (user_id, blog_id))
            result = cursor.fetchone()

    return False if result[0] is None else True


def does_comment_belong_to_user(user_id, comment_id):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
            cursor.callproc('blog.does_comment_belong_to_user', (user_id, comment_id))
            result = cursor.fetchone()

    return False if result[0] is None else True


def admin_delete(request, id):
    if request.method == "POST":
        role = request.session.get("role")
        if role is None:
            return redirect('login', error="error")
        elif role != "admin":
            return HttpResponseForbidden("You have no roots!")
        else:
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.callproc('blog.delete_user', (int(id), ))

            return redirect('admin_user')
    else:
        return redirect('index')


def admin_edit(request, id):
    role = request.session.get("role")
    if role is None:
        return redirect('login', error="error")
    elif role != "admin":
        return HttpResponseForbidden("You have no roots!")
    else:
        pass
        # with psycopg2.connect(settings.DSN) as conn:
        #     with conn.cursor() as cursor:
        #         cursor.execute('SELECT * from blog.record')
        #         logs = cursor.fetchall()

        return render(request, 'bloghost/account_edit.html')


def admin_log(request):
    role = request.session.get("role")
    if role is None:
        return redirect('login', error="error")
    elif role != "admin":
        return HttpResponseForbidden("You have no roots!")
    else:
        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT * from blog.record')
                logs = cursor.fetchall()

        return render(request, 'bloghost/admin_logs.html', {'logs': logs})


def admin_users(request):
    role = request.session.get("role")
    if role is None:
        return redirect('login', error="error")
    elif role != "admin":
        return HttpResponseForbidden("You have no roots!")
    else:
        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT * from blog."User"')
                users = cursor.fetchall()

        return render(request, 'bloghost/admin_users.html', {'users': users})


def edit_user(request, id):
    user_id = request.session.get("user_id")
    if user_id is not None:
        if user_id != int(id):
            return redirect('index', error="It is not your profile!")
        if request.method == "POST":
            form = UserForm(request.POST)
            if form.is_valid():
                with psycopg2.connect(settings.DSN) as conn:
                    with conn.cursor() as cursor:
                        cursor.callproc(
                            'blog.update_user',
                            {
                                'id': form.cleaned_data["Id"],
                                'first_name': form.cleaned_data["first_name"],
                                'second_name': form.cleaned_data["second_name"],
                                'birth_date': form.cleaned_data["birth_date"],
                                'email': form.cleaned_data["email"],
                                'login': form.cleaned_data["login"]
                            }
                        )
                        user = cursor.fetchone()
                        request.session["name"] = form.cleaned_data["first_name"] + ' ' + form.cleaned_data["second_name"]
                        return redirect('detail_user', id = user[0])
        else:
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        'SELECT * from blog."User" where id = %s', (id,))
                    user = cursor.fetchone()
                    print user
            data = {'Id': user[0], 'first_name': user[2], 'second_name': user[1], 'birth_date': user[3], 'login': user[6], 'email': user[4]}
            form = UserForm(initial=data)

        return render(request, 'bloghost/edit_user.html', {'form': form, 'id':id})
    else:
        return redirect('login', error="error")


def edit_blog(request, id):
    user_id = request.session.get("user_id")

    if user_id is not None:
        if not does_blog_belong_to_user(user_id, int(id)):
            return redirect('index', error="It is not your blog")

        if request.method == "POST":
            form = BlogForm(request.POST)
            if form.is_valid():
                print form.cleaned_data
                with psycopg2.connect(settings.DSN) as conn:
                    with conn.cursor() as cursor:
                        cursor.callproc(
                            'blog.update_blog',
                            {
                                'id': form.cleaned_data["Id"],
                                'name': form.cleaned_data["Name"]
                            }
                        )
                        user = cursor.fetchone()

                        return redirect('detail_blog', id = user[0])
        else:
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        'SELECT * from blog.blog where id = %s', (id,))
                    user = cursor.fetchone()
                    print user
            data = {'Id': user[0], 'Name': user[1]}
            form = BlogForm(initial=data)

        return render(request, 'bloghost/edit_blog.html', {'form': form, 'id':id})
    else:
        return redirect('login', error="error")


def edit_post(request, id):
    user_id = request.session.get("user_id")
    if user_id is not None:
        if not does_publ_belong_to_user(user_id, int(id)):
            return redirect('index', error="It is not your post")

        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                cursor.callproc('blog.select_blogs', (user_id,))
                blogs = cursor.fetchall()

                cursor.execute(
                    'SELECT * from blog.publication where id = %s', (id,))
                post = cursor.fetchone()

        data = {'ID': post[0], 'Text': post[1], 'Name': post[2], 'Blog_ID': post[3]}
        if request.method == "POST":
            form = PostForm(blogs, request.POST)
            if form.is_valid():
                print form.cleaned_data
                with psycopg2.connect(settings.DSN) as conn:
                    with conn.cursor() as cursor:
                        cursor.callproc(
                            'blog.update_publication',
                            {
                                'id': form.cleaned_data["ID"],
                                'Text': form.cleaned_data["Text"],
                                'name': form.cleaned_data["Name"],
                                'blog_id': form.cleaned_data["Blog_ID"]
                            }
                        )
                        post = cursor.fetchone()
                        return redirect('detail_post', id = post[0])
        else:
            form = PostForm(blogs, initial=data)

        return render(request, 'bloghost/edit_user.html', {'form': form, 'id': id})
    else:
        return redirect('login', error="error")

# Done
def create_comment(request):
    if request.session.get("user_id") is not None:
        if request.method == "POST":
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.callproc(
                        'blog.create_comment',
                        {
                            'text': request.POST["text"],
                            'user_id': request.POST["user_id"],
                            'publication_id': request.POST["post_id"],
                            'publication_blog_idblog': request.POST["blog_id"]
                        }
                    )

                    return redirect('detail_post', id = request.POST["post_id"])

        return redirect('index')
    else:
        return redirect('login', error="error")


def create_blog(request):
    if request.session.get("user_id") is not None:
        if request.method == "POST":
            form = BlogForm(request.POST)
            if form.is_valid():
                with psycopg2.connect(settings.DSN) as conn:
                    with conn.cursor() as cursor:
                        cursor.callproc(
                            'blog.create_blog',
                            {
                                'name': form.cleaned_data["Name"],
                                'user_id': request.session["user_id"]
                            }
                        )
                        blog = cursor.fetchone()

                        return redirect('detail_blog', id = blog[0])
        else:
            form = BlogForm()

        return render(request, 'bloghost/new_blog.html', {'form': form})
    else:
        return redirect('login', error="error")


def create_post(request):
    if request.session.get("user_id") is not None:
        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                cursor.callproc('blog.select_blogs', (request.session.get("user_id"),))
                blogs = cursor.fetchall()

        if request.method == "POST":
            form = PostForm(blogs, request.POST)
            if form.is_valid():
                print form.cleaned_data
                with psycopg2.connect(settings.DSN) as conn:
                    with conn.cursor() as cursor:
                        cursor.callproc(
                            'blog.create_publication',
                            {
                                'text': form.cleaned_data["Text"],
                                'name': form.cleaned_data["Name"],
                                'blog_id': form.cleaned_data["Blog_ID"]
                            }
                        )
                        post = cursor.fetchone()
                        return redirect('detail_post', id = post[0])
        else:
            form = PostForm(blogs)

        return render(request, 'bloghost/new_post.html', {'form': form})
    else:
        return redirect('login', error="error")

# Done
def delete_blog(request, id):
    user_id = request.session.get("user_id")

    if user_id is not None:
        if not does_blog_belong_to_user(user_id, int(id)):
            return redirect('index', error="It is not your blog")
        if request.method == "POST":
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        'UPDATE blog.blog SET isdeleted=true WHERE id = %s',
                        (id, )
                    )

            return redirect('index')

        return render(request, 'bloghost/delete_blog.html', {'id': id, 'item': 'blog'})
    else:
        return redirect('login', error="error")

# Done
def delete_post(request, id):
    user_id = request.session.get("user_id")
    if user_id is not None:
        if not does_publ_belong_to_user(user_id, int(id)):
            return redirect('index', error="It is not your post")
        if request.method == "POST":
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        'UPDATE blog."publication" SET isdeleted=true WHERE id = %s',
                        (id, )
                    )

            return redirect('index')

        return render(request, 'bloghost/delete_post.html', {'id': id, 'item': 'post'})
    else:
        return redirect('login', error="error")

# Done
def delete_comment(request, id):
    user_id = request.session.get("user_id")
    if user_id is not None:
        if not does_comment_belong_to_user(user_id, int(id)):
            return redirect('index', error="It is not your comment")
        if request.method == "POST":
            with psycopg2.connect(settings.DSN) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        'UPDATE blog."Comment" SET isdeleted=true WHERE id = %s',
                        (id, )
                    )

            return redirect('index')

        return render(request, 'bloghost/delete_comment.html', {'id': id, 'item': 'comment'})
    else:
        return redirect('login', error="error")

# Done
def index(request, error=None):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
           cursor.callproc('blog.select_publication')
           records = cursor.fetchall()

    if error is not None:
        return render(
            request, 'bloghost/index_error.html',
            {'records': records, 'info': 'Recent publications', 'message': error}
        )

    return render(
        request,
        'bloghost/index.html',
        {'records': records, 'info': 'Recent publications'}
    )

#  add user deleteion
def detail_user(request, id):
    session_id = request.session.get("user_id")
    if session_id == int(id):
        match = True
    else:
        match = False

    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT id, first_name, second_name, birth_date, email, "Role"
    	        FROM blog."User" where id = (%s);
            """, (id, ))
            user = cursor.fetchone()

            cursor.callproc('blog.select_blogs', (id, ))
            blogs = cursor.fetchall()

            cursor.callproc('blog.select_publication_by_user_id', (id, ))
            pubs = cursor.fetchall()

    return render(
        request, 'bloghost/user.html',
        {'blogs': blogs, 'records': pubs, 'match': match, 'user': user}
    )

# DONE
def detail_blog(request, id):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
            cursor.execute('select name from blog.blog where id = %s', (id, ))
            name = cursor.fetchone()[0]

            cursor.callproc('blog.select_publics_by_blog_id', (id, ))
            publics = cursor.fetchall()

    return render(
        request,
        'bloghost/index.html',
        {'records': publics, 'info': 'Blog {}'.format(name)}
    )

# Comment deleteion
def detail_post(request, id):
    with psycopg2.connect(settings.DSN) as conn:
        with conn.cursor() as cursor:
           cursor.callproc('blog.select_publication_by_id', (id, ))
           post = cursor.fetchone()

           cursor.callproc('blog.select_comments_by_pub_id', (id, ))
           comments = cursor.fetchall()

    return render(
        request,
        'bloghost/post.html',
        {'post': post, 'comments': comments, 'id': id}
    )

# DONE
def login(request, error=None):
    if request.method == 'POST':
        login = request.POST.get("lg_username")
        password = hashlib.md5(request.POST["lg_password"]).hexdigest()
        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                cursor.callproc('blog.confirm_user', (login, password))
                user = cursor.fetchone()

                if not user:
                    return redirect('login', error="error")
                else:
                    request.session["user_id"] = user[0]
                    request.session["role"] = user[1]
                    request.session["name"] = user[2]
                    return redirect('index')
    else:
        if error is not None:
            return render(request, 'bloghost/login_error.html')

        return render(request, 'bloghost/login.html')

# DONE
def logout(request):
    request.session.flush()

    return redirect('index')

# DONE
def register(request, error=None):
    if request.method == 'POST':
        error_message = ""

        login = request.POST["login"]
        password = request.POST["reg_password"]
        pass_confirm = request.POST["reg_password_confirm"]

        if not login or not password:
            error_message = "Provide login and password"
        if password != pass_confirm:
            error_message = "Password confirmation is failed"

        # TODO: check user by login if not == uniques in db, reg_email - unique?

        if error_message:
            return redirect('register', error=error_message)

        password = hashlib.md5(request.POST["reg_password"]).hexdigest()

        first_name = request.POST["first_name"]
        second_name = request.POST["last_name"]
        email = request.POST["reg_email"]

        with psycopg2.connect(settings.DSN) as conn:
            with conn.cursor() as cursor:
                # TODO return id and what is
                # TODO add birth_date
                cursor.callproc('blog.create_user',
                    {
                        'first_name': first_name, 'second_name': second_name,
                        'birth_date': '1998-08-21', 'email': email,
                        'login': login, 'password': password
                    }
                )
                result = cursor.fetchone()
                request.session["user_id"] = result[0]
                request.session["role"] = result[1]
                request.session["name"] = result[2]

                return redirect('index')

        return HttpResponse("You're logged in.")

    if error is not None:
        return render(
            request, 'bloghost/register_error.html',
            {'message': error}
        )

    return render(request, 'bloghost/register.html')
