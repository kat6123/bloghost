from django import forms

class UserForm(forms.Form):
    Id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    first_name = forms.CharField(label='First name', max_length=100)
    second_name = forms.CharField(label='Second name', max_length=100)
    birth_date = forms.DateField()
    login = forms.CharField(label='Login', max_length=100)
    email = forms.EmailField(label='Email')


class BlogForm(forms.Form):
    Id = forms.IntegerField(widget=forms.HiddenInput(), required=False) # ? required
    Name = forms.CharField(max_length=45, min_length=1, strip=True)


class PostForm(forms.Form):
    def __init__(self, blogs, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['Blog_ID'] = forms.ChoiceField(
            choices=[(blog[0], blog[1]) for blog in blogs],
            label='Blog'
        )

    ID = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    Text = forms.CharField(widget=forms.Textarea)
    Name = forms.CharField(max_length=45, min_length=1, strip=True)
