from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(r'^index/(?P<error>[\w !]+)/$', views.index, name = 'index'),

    url(r'^admin/log/$', views.admin_log, name = 'admin_log'),
    url(r'^admin/users/$', views.admin_users, name = 'admin_user'),
    url(r'^admin/edit/(?P<id>[0-9]+)/$', views.admin_edit, name = 'admin_edit'),
    url(r'^admin/delete/(?P<id>[0-9]+)/$', views.admin_delete, name = 'admin_delete'),

    url(r'^new/blog/$', views.create_blog, name = 'create_blog'),
    url(r'^new/post/$', views.create_post, name = 'create_post'),
    url(r'^new/comment/$', views.create_comment, name = 'create_comment'),
    url(r'^delete/blog(?P<id>[0-9]+)/$', views.delete_blog, name = 'delete_blog'),
    url(r'^delete/post(?P<id>[0-9]+)/$', views.delete_post, name = 'delete_post'),
    url(r'^delete/comment(?P<id>[0-9]+)/$', views.delete_comment, name = 'delete_comment'),
    url(r'^blog(?P<id>[0-9]+)/$', views.detail_blog, name = 'detail_blog'),
    url(r'^post(?P<id>[0-9]+)/$', views.detail_post, name = 'detail_post'),
    url(r'^user(?P<id>[0-9]+)/$', views.detail_user, name = 'detail_user'),
    url(r'^blog(?P<id>[0-9]+)/edit/$', views.edit_blog, name = 'edit_blog'),
    url(r'^post(?P<id>[0-9]+)/edit/$', views.edit_post, name = 'edit_post'),
    url(r'^user(?P<id>[0-9]+)/edit/$', views.edit_user, name = 'edit_user'),

    url(r'^login/$', views.login, name = 'login'),
    url(r'^login/(?P<error>\w+)/$', views.login, name = 'login'),

    url(r'^logout/$', views.logout, name = 'logout'),

    url(r'^register/$', views.register, name = 'register'),
    url(r'^register/(?P<error>[\w ]+)/$', views.register, name = 'register'),
]
